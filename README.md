# Memo Team Bundle

## Table of contents

* [Memo Team Bundle](#markdown-header-memo-team-bundle)
  * [Table of contents](#markdown-header-table-of-contents)
  * [About](#markdown-header-about)
  * [Installation](#markdown-header-installation)
  * [Usage](#markdown-header-usage)
  * [Features](#markdown-header-features)
  * [Hooks](#markdown-header-hooks)
  * [Requirements](#markdown-header-requirements)
  * [Known issues](#markdown-header-known-issues)
  * [Contributing / Reporting issues](#markdown-header-contributing-reporting-issues)
  * [License](#markdown-header-license)

## About

With the Team Bundle you can add different list and detail-views of teams/team-members.

## Installation

Install [composer](https://getcomposer.org) if you haven't already. Add the unlisted Repo (not on packagist.org) to your
composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-foundation-bundle.git"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-category-bundle.git"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-team-bundle.git"
  }
],
```

Add the bundle to your requirements:

```
"memo_development/contao-foundation-bundle": "^2.0",
"memo_development/contao-category-bundle": "^2.0",
"memo_development/contao-team-bundle": "^2.0",
```

Update the database via contao install tool or console:

```
./update-db.sh
```

## Usage

* Create a Team-Listing Page
* Create a Team-Detail Page
* Create a new Team-Archive and link both pages from above
* Create a new Person in the Team-Archive
* Create a new Team-Listing-Content-Element or Module (and configure it)
* Create a new Team-Detail-Module
* Give the backend users rights to manage the team-archives
* Optional: Create a copy of templates (team_item, team_item_full) and modify them
* Optional: Style the output with css

## Features

* Backend management of persons (with firstname, lastname, image, gallery, description, phone, e-mail, category, etc.)
* List of all persons in one or more archives
* List of all persons by category
* List of all persons by custom/manual selection
* List of all persons by sql statement
* Auto include to portfolio bundle
* As Element or Module
* Contao URL-Picker for easy linking to detailpages
* Auto add detailpages to sitemap
* Detailpage of a person

## Hooks
* generateTeamDCA - Customize DCA (add, remove or change fields/config) **before** the fields get multiplied for translation (so no title_en, title_fr yet), expects the DCA as array as return value
* generateTeamDCAComplete - Customize the final DCA (remove fields, change fields) **after** the language fields have been generated (so title_en, title_fr are available), expects the DCA as array as return value
* addItemToSitemap - Customize how and if urls are added to the sitemap, expects the url as string as return value (or false, to remove the url)
  

## Requirements

* PHP 8.1+
* Contao 5.2+
* Foundation-Bundle
* Category-Bundle

## Known Issues

* From version 1.0.7 to 1.1.0, the filename of the template for the content element `team_listing_custom`
  and `team_listing_archive` changed from `ce_team_listing_custom` to `ce_team_listing`.
  __If you modified the template__ you will need to rename it to `ce_team_listing` and/or copy it under the new
  filename. Any other templates in the group __won't__ be affected as they still match the naming convention.

## Contributing / Reporting issues

Bug reports and pull requests are welcome - via Bitbucket-Issues:
[Bitbucket Issues](https://bitbucket.org/memo_development/contao-team-bundle/issues?status=new&status=open)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
