<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoTeamBundle extends Bundle
{
}
