<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\FrontendModule;

use Memo\FoundationBundle\Module\FoundationModule;
use Memo\TeamBundle\Model\TeamModel;

class TeamListingCustom extends FoundationModule
{
    /**
     * Table
     * @var string
     */
    protected static $strTable = 'tl_memo_team';

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_team_listing_custom';

    protected function compile()
    {
        $arrItems = unserialize($this->foundation_item_selection);

        // Retrieve Items
        $t = static::$strTable;

        $colItems = TeamModel::findPublishedByIds($arrItems, [
            'order' => $this->Database::getInstance()->findInSet("$t.id", $arrItems),
        ]);

        if ($this->size) {
            $this->imgSize = $this->size;
        }

        if (is_object($colItems)) {
            $arrItems = $this->parseItems($colItems);

            $this->Template->items = $arrItems;
        }

        if ($this->customTpl) {
            $this->Template->strTemplate = $this->customTpl;
        }
    }
}
