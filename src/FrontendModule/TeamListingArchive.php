<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\FrontendModule;

use Memo\TeamBundle\Module\ModuleTeamListing;

class TeamListingArchive extends ModuleTeamListing
{
    /**
     * Template
     * @var string
     */

    protected $strTemplate = 'ce_team_listing';

}
