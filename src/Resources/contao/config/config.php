<?php declare(strict_types=1);

use Contao\System;
use Contao\TableWizard;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
$GLOBALS['BE_MOD']['content']['memo_team'] = array
(
    'tables' => array(
        'tl_memo_team_archive',
        'tl_memo_team'
    ),
    'table' => array(TableWizard::class, 'importTable'),
);

/**
 * Content elements
 */
$GLOBALS['TL_CTE']['team']['team_listing_archive'] = 'Memo\TeamBundle\FrontendModule\TeamListingArchive';;
$GLOBALS['TL_CTE']['team']['team_listing_custom'] = 'Memo\TeamBundle\FrontendModule\TeamListingCustom';;

/**
 * Add front end modules
 */
$GLOBALS['FE_MOD']['memo_team'] = array
(
    'team_listing' => 'Memo\TeamBundle\Module\ModuleTeamListing',
    'team_reader' => 'Memo\TeamBundle\Module\ModuleTeamReader',
);


/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_team_archive'] = 'Memo\TeamBundle\Model\TeamArchiveModel';
$GLOBALS['TL_MODELS']['tl_memo_team'] = 'Memo\TeamBundle\Model\TeamModel';

/**
 * Backend CSS
 */
if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))) {
    $GLOBALS['TL_CSS'][] = 'bundles/memoteam/backend.css?v=2020-08-08';
}

/**
 * AutoItem for Translation of aliases
 */
$GLOBALS['TL_AUTO_ITEM'][] = 'alias';

/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'teams';
$GLOBALS['TL_PERMISSIONS'][] = 'teamp';
