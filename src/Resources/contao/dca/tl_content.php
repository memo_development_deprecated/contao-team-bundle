<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

\Contao\Controller::loadDataContainer('tl_module');
\Contao\System::loadLanguageFile('tl_module');

/**
 * Table tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['team_listing_archive'] = '{type_legend},type,headline; {source_legend}, foundation_archives, foundation_featured, categories_filter, categories_filter_type, sql_filter, foundation_order; {image_legend}, size; {template_legend:hide}, customTpl, foundation_item_template, ; {invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['team_listing_custom'] = '{type_legend},type,headline; {source_legend}, foundation_archives, foundation_item_selection; {image_legend}, size;{template_legend:hide}, customTpl, foundation_item_template ; {invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = array('tl_content_team', 'autoOverrideOptions');

use Contao\Backend;
use Contao\ContentModel;

/**
 * Class tl_content
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_content_team extends Backend
{
    public function autoOverrideOptions($dc)
    {
        $objContentItem = ContentModel::findByPk($dc->id);

        if (!is_null($objContentItem) && !is_null($objContentItem->type) && stristr($objContentItem->type, 'team')) {

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives']['options_callback'] = array('tl_module_team', 'getArchives');

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_template']['options_callback'] = array('tl_module_team', 'getTemplates');

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_selection']['options_callback'] = array('tl_module_team', 'getSelectionOptions');
        }
    }
}

