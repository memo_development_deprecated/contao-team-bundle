<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

// Extend the default palettes
PaletteManipulator::create()
    ->addLegend('team_legend', 'amg_legend', PaletteManipulator::POSITION_BEFORE)
    ->addField(array('teams', 'teamp'), 'team_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('extend', 'tl_user')
    ->applyToPalette('custom', 'tl_user');

// Add fields to tl_user
$GLOBALS['TL_DCA']['tl_user']['fields']['teams'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_user']['teams'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_memo_team_archive.title',
    'eval' => array('multiple' => true),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user']['fields']['teamp'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_user']['teamp'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => array('create', 'delete'),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => array('multiple' => true),
    'sql' => "blob NULL"
);
