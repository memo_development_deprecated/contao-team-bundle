<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\Security\ContaoCorePermissions;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Contao\BackendUser;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Config;
use Contao\DC_Table;
use Contao\UserGroupModel;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Table tl_memo_team_archive
 */
$GLOBALS['TL_DCA']['tl_memo_team_archive'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => DC_Table::class,
        'enableVersioning' => true,
        'ctable' => array(
            'tl_memo_team'
        ),
        'switchToEdit' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
        'onload_callback' => array
        (
            array('tl_memo_team_archive', 'checkPermission')
        ),
        'oncreate_callback' => array
        (
            array('tl_memo_team_archive', 'adjustPermissions')
        ),
        'oncopy_callback' => array
        (
            array('tl_memo_team_archive', 'adjustPermissions')
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 1,
            'fields' => array('title'),
            'flag' => 1,
            'panelLayout' => 'filter; search; limit'
        ),
        'label' => array
        (
            'fields' => array('title', 'alias'),
            'format' => '%s <span class="backend-info">[%s]</span>'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'href' => 'act=edit',
                'icon' => 'edit.svg',
                'button_callback' => array('tl_memo_team_archive', 'editHeader')
            ),
            'children',
            'copy' => array
            (
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'button_callback' => array('tl_memo_team_archive', 'copyItem')
            ),
            'delete' => array
            (
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
                'button_callback' => array('tl_memo_team_archive', 'deleteItem')
            ),
            'toggle' => array
            (
                'href' => 'act=toggle&amp;field=published',
                'icon' => 'visible.svg',
                'showInHeader' => true
            ),
            'show'
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend}, title, alias, category_groups, jumpTo, jumpToListing, teaser; {media_legend}, singleSRC; {meta_legend}, robots; {publish_legend}, published, start, stop;',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'alias' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['alias'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'save_callback' => array
            (
                array('tl_memo_team_archive', 'generateAlias')
            ),
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ),
        'jumpTo' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['jumpTo'],
            'translate' => true,
            'exclude' => true,
            'inputType' => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval' => array(
                'fieldType' => 'radio',
                'tl_class' => 'clr m12'
            ),
            'sql' => "int(10) unsigned NOT NULL default 0",
            'relation' => array(
                'type' => 'hasOne',
                'load' => 'lazy'
            )
        ),
        'jumpToListing' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['jumpToListing'],
            'translate' => true,
            'exclude' => true,
            'inputType' => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval' => array(
                'fieldType' => 'radio',
                'tl_class' => 'clr'
            ),
            'sql' => "int(10) unsigned NOT NULL default 0",
            'relation' => array(
                'type' => 'hasOne',
                'load' => 'lazy'
            )
        ),
        'category_groups' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['category_groups'],
            'exclude' => true,
            'filter' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_memo_category.title',
            'options_callback' => ['memo.foundation.category', 'getCategoryGroups'],
            'eval' => array(
                'multiple' => true,
                'chosen' => true,
                'tl_class' => 'clr long'
            ),
            'sql' => "blob NULL"
        ),
        'teaser' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'singleSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['singleSRC'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'filesOnly' => true,
                'extensions' => Config::get('validImageTypes'),
                'fieldType' => 'radio'
            ),
            'sql' => "binary(16) NULL"
        ),
        'robots' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['robots'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'select',
            'options' => array(
                'index,follow',
                'index,nofollow',
                'noindex,follow',
                'noindex,nofollow'
            ),
            'eval' => array(
                'tl_class' => 'w50'
            ),
            'sql' => "varchar(32) NOT NULL default ''"
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['published'],
            'translate' => false,
            'exclude' => true,
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50'
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'start' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['start'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard clr'),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team_archive']['stop'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'sql' => "varchar(10) NOT NULL default ''"
        )
    )
);

// Add the translated-fields for all 'translate' dca-fields
$objLanguageService = System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_team_archive');

/**
 * Class tl_memo_team_archive
 */
class tl_memo_team_archive extends FoundationBackend
{
    public function __construct()
    {
        parent::__construct();
        $this->import(BackendUser::class, 'User');
    }

    /**
     * Check permissions to edit table tl_memo_team_archive
     *
     * @throws AccessDeniedException
     */
    public function checkPermission()
    {

        // Allow Admins
        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->teams) || !is_array($this->User->teams)) {
            $root = array(0);
        } else {
            $root = $this->User->teams;
        }

        $GLOBALS['TL_DCA']['tl_memo_team_archive']['list']['sorting']['root'] = $root;

        // Check permissions to add archives
        if (!$this->User->hasAccess('create', 'teamp')) {
            $GLOBALS['TL_DCA']['tl_memo_team_archive']['config']['closed'] = true;
            $GLOBALS['TL_DCA']['tl_memo_team_archive']['config']['notCreatable'] = true;
            $GLOBALS['TL_DCA']['tl_memo_team_archive']['config']['notCopyable'] = true;
        }


        // Check permissions to delete calendars
        if (!$this->User->hasAccess('delete', 'teamp')) {
            $GLOBALS['TL_DCA']['tl_memo_team_archive']['config']['notDeletable'] = true;
        }

        // Get request (contao 5+)
        $objRequest = System::getContainer()->get('request_stack')->getCurrentRequest();

        // Get session (contao 5+)
        $objSession = $objRequest->getSession();

        // Check current action
        switch (Input::get('act')) {
            case 'select':
                // Allow
                break;

            case 'create':
                if (!$this->User->hasAccess('create', 'teamp')) {
                    throw new AccessDeniedException('Not enough permissions to create team archives.');
                }
                break;

            case 'edit':
            case 'copy':
            case 'delete':
            case 'show':
                if (!in_array(Input::get('id'), $root) || (Input::get('act') == 'delete' && !$this->User->hasAccess('delete', 'teamp'))) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' team archive ID ' . Input::get('id') . '.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
            case 'copyAll':
                $session = $objSession->all();

                if (Input::get('act') == 'deleteAll' && !$this->User->hasAccess('delete', 'teamp')) {
                    $session['CURRENT']['IDS'] = array();
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array)$session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (Input::get('act')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' teams archives.');
                }
                break;
        }
    }

    /**
     * Add the new archive to the permissions
     *
     * @param $insertId
     */
    public function adjustPermissions($insertId)
    {
        // The oncreate_callback passes $insertId as second argument
        if (func_num_args() == 4) {
            $insertId = func_get_arg(1);
        }

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->teams) || !is_array($this->User->teams)) {
            $root = array(0);
        } else {
            $root = $this->User->teams;
        }

        // The archive is enabled already
        if (in_array($insertId, $root)) {
            return;
        }

        /** @var AttributeBagInterface $objSessionBag */
        $objSessionBag = System::getContainer()->get('session')->getBag('contao_backend');

        $arrNew = $objSessionBag->get('new_records');

        if (is_array($arrNew['tl_memo_team_archive']) && in_array($insertId, $arrNew['tl_memo_team_archive'])) {
            // Add the permissions on group level
            if ($this->User->inherit != 'custom') {
                $objGroup = $this->Database->execute("SELECT id, teams, teamp FROM tl_user_group WHERE id IN(" . implode(',', array_map('\intval', $this->User->groups)) . ")");

                while ($objGroup->next()) {
                    $arrTeamp = StringUtil::deserialize($objGroup->teamp);

                    if (is_array($arrTeamp) && in_array('create', $arrTeamp)) {
                        $arrTeams = StringUtil::deserialize($objGroup->teams, true);
                        $arrTeams[] = $insertId;

                        $this->Database->prepare("UPDATE tl_user_group SET teams=? WHERE id=?")
                            ->execute(serialize($arrTeams), $objGroup->id);
                    }
                }
            }

            // Add the permissions on user level
            if ($this->User->inherit != 'group') {
                $objUser = $this->Database->prepare("SELECT teams, teamp FROM tl_user WHERE id=?")
                    ->limit(1)
                    ->execute($this->User->id);

                $arrTeamp = StringUtil::deserialize($objUser->teamp);

                if (is_array($arrTeamp) && in_array('create', $arrTeamp)) {
                    $arrTeams = StringUtil::deserialize($objUser->teams, true);
                    $arrTeams[] = $insertId;

                    $this->Database->prepare("UPDATE tl_user SET teams=? WHERE id=?")
                        ->execute(serialize($arrTeams), $this->User->id);
                }
            }

            // Add the new element to the user object
            $root[] = $insertId;
            $this->User->teams = $root;
        }
    }

    /**
     * Return the copy archive button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function copyArchive($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('create', 'teamp') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
    }

    /**
     * Return the delete archive button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function deleteArchive($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('delete', 'teamp') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
    }

    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        return System::getContainer()->get('security.helper')->isGranted(ContaoCorePermissions::USER_CAN_EDIT_FIELDS_OF_TABLE, 'tl_memo_team_archive') ? '<a href="' . self::addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(str_replace('.svg', '--disabled.svg', $icon)) . ' ';
    }

    public function copyItem($row, $href, $label, $title, $icon, $attributes)
    {
        return System::getContainer()->get('security.helper')->isGranted(ContaoCorePermissions::USER_CAN_EDIT_FIELDS_OF_TABLE, 'tl_memo_team_archive') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(str_replace('.svg', '--disabled.svg', $icon)) . ' ';
    }

    public function deleteItem($row, $href, $label, $title, $icon, $attributes)
    {
        return System::getContainer()->get('security.helper')->isGranted(ContaoCorePermissions::USER_CAN_EDIT_FIELDS_OF_TABLE, 'tl_memo_team_archive') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(str_replace('.svg', '--disabled.svg', $icon)) . ' ';
    }

    public function generateAlias($varValue, DataContainer $dc)
    {
        return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_team_archive');
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
    }

    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        $this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_team_archive', 'published');
    }
}
