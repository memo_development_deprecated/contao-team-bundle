<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\DataContainer;
use Contao\DC_Table;
use Contao\System;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Memo\TeamBundle\Model\TeamModel;

/**
 * Table tl_memo_team
 */
$GLOBALS['TL_DCA']['tl_memo_team'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => DC_Table::class,
        'ptable' => 'tl_memo_team_archive',
        'switchToEdit' => true,
        'enableVersioning' => true,
        'markAsCopy' => 'title',
        'onsubmit_callback' => array(array('tl_memo_team', 'generateTitle')),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid,sorting' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 4,
            'fields' => array('sorting'),
            'panelLayout' => 'filter;sort,search,limit',
            'headerFields' => array('title', 'alias'),
            'child_record_callback' => array(
                'tl_memo_team', 'listLayout'
            ),
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit',
            'copy',
            'cut',
            'delete',
            'feature' => array
            (
                'href' => 'act=toggle&amp;field=featured',
                'icon' => 'featured.svg',
            ),
            'toggle' => array
            (
                'href' => 'act=toggle&amp;field=published',
                'icon' => 'visible.svg',
                'showInHeader' => true
            ),
            'show'
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '
            {general_legend}, firstname, lastname, categories, subtitle, alias, description;
            {meta_legend},seo_title,seo_description,serpPreview;
            {contact_legend}, phone, mobile, email;
            {media_legend},singleSRC, multiSRC;
            {date_legend}, date;
            {publish_legend}, featured, published, start, stop;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'foreignKey' => 'tl_memo_team_archive.id',
            'sql' => "int(10) unsigned NOT NULL",
            'relation' => array('type' => 'belongsTo', 'load' => 'lazy')
        ),
        'sorting' => array
        (
            'label' => &$GLOBALS['TL_LANG']['MSC']['sorting'],
            'sorting' => true,
            'flag' => 11,
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['title'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50',
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'lastname' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['lastname'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50',
                'submitOnChange' => true
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'firstname' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['firstname'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50',
                'submitOnChange' => true
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'subtitle' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['subtitle'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'clr w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'alias' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['alias'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'save_callback' => array
            (
                array('tl_memo_team', 'generateAlias')
            ),
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ),
        'phone' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['phone'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'maxlength' => 255,
                'tl_class' => 'w50 clr',
                'allowHtml' => true

            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'mobile' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['mobile'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'maxlength' => 255,
                'tl_class' => 'w50',
                'allowHtml' => true
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50 clr'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'description' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'singleSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['singleSRC'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'filesOnly' => true,
                'extensions' => '%contao.image.valid_extensions%',
                'fieldType' => 'radio'),
            'sql' => "binary(16) NULL"
        ),
        'multiSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['multiSRC'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => array(
                'multiple' => true,
                'fieldType' => 'checkbox',
                'orderField' => 'orderSRC',
                'files' => true,
                'extensions' => '%contao.image.valid_extensions%',
                'isGallery' => true,
                'tl_class' => 'clr m12'
            ),
            'sql' => "blob NULL"
        ),
        'orderSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['orderSRC'],
            'sql' => "blob NULL"
        ),
        'categories' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['categories'],
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => array(
                'tl_class' => 'clr',
                'style' => 'width:710px; max-width: 100%;',
                'dragAndDrop' => false,
                'columnFields' => array(
                    'category_id' => array(
                        'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['category_id'],
                        'exclude' => true,
                        'inputType' => 'select',
                        'options_callback' => array('memo.foundation.category', 'getCategoriesByDC'),
                        'eval' => array(
                            'style' => 'width:250px',
                            'includeBlankOption' => true,
                            'multiple' => false,
                        ),
                    ),
                    'category_fk_meta' => array(
                        'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['category_fk_meta'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => array(
                            'style' => 'width:400px'
                        ),
                    ),
                ),
            ),
            'sql' => "blob NULL"
        ),
        'date' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['date'],
            'default' => time(),
            'exclude' => true,
            'filter' => false,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'date', 'mandatory' => true, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'load_callback' => array
            (
                array('memo.foundation.toolbox', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'seo_title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['seo_title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 128, 'tl_class' => 'clr'),
            'sql' => "varchar(128) NOT NULL default ''",
        ),
        'seo_description' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['seo_description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'serpPreview' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['serpPreview'],
            'exclude' => true,
            'inputType' => 'serpPreview',
            'eval' => array('url_callback' => array('tl_memo_team', 'getSerpUrl'), 'titleFields' => array('seo_title', 'title', 'lastname'), 'descriptionFields' => array('seo_description', 'description')),
            'sql' => null
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['published'],
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50 clr'
            ),
            'sql' => array('type' => 'boolean', 'default' => false)
        ),
        'featured' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['featured'],
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50'
            ),
            'sql' => array('type' => 'boolean', 'default' => false)
        ),
        'start' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['start'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard clr'),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_team']['stop'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'sql' => "varchar(10) NOT NULL default ''"
        )
    )
);

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateTeamDCA']) && \is_array($GLOBALS['TL_HOOKS']['generateTeamDCA'])) {
    foreach ($GLOBALS['TL_HOOKS']['generateTeamDCA'] as $callback) {
        $this->import($callback[0]);
        $GLOBALS['TL_DCA']['tl_memo_team'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_team'], $this);
    }
}

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_team');


$objToolboxService = System::getContainer()->get('memo.foundation.toolbox');
$objToolboxService->toggleDetailFieldsFromPalette('tl_memo_team');

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateTeamDCAComplete']) && \is_array($GLOBALS['TL_HOOKS']['generateTeamDCAComplete'])) {
    foreach ($GLOBALS['TL_HOOKS']['generateTeamDCAComplete'] as $callback) {
        $this->import($callback[0]);
        $GLOBALS['TL_DCA']['tl_memo_team'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_team'], $this);
    }
}

/**
 * Class tl_memo_team
 */
class tl_memo_team extends FoundationBackend
{
    public function getSerpUrl($objItem)
    {
        return $this->generateSerpUrl($objItem);
    }

    public function generateTitle(DataContainer $dc)
    {
        // Get the current item
        $objItem = TeamModel::findByPk($dc->activeRecord->id);

        // Set the new title - based on firstname and lastname
        $objItem->title = $dc->activeRecord->firstname . ' ' . $dc->activeRecord->lastname;

        // Save the title
        $objItem->save();
    }


    public function listLayout($row)
    {
        $strTitle = '<div class="tl_content_left">' . $row['firstname'] . ' ' . $row['lastname'];

        if (isset($row['email']) && $row['email'] != '') {
            $strTitle .= ' <span style="color:#b3b3b3; padding-left:3px;">[' . $row['email'] . ']</span>';
        }

        if (isset($row['categories'])) {
            $strTitle .= self::getCategoryLabels($row['categories']);
        }

        $strTitle .= '</div>';

        return $strTitle;
    }

    public function generateAlias($varValue, DataContainer $dc)
    {
        return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_team', array('title'));
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
    }

    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        $this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_team', 'published');
    }
}
