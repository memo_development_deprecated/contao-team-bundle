<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Memo\TeamBundle\Model\TeamArchiveModel;
use Memo\TeamBundle\Model\TeamModel;
use Contao\Backend;
use Contao\ModuleModel;
use Contao\DataContainer;

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['team_listing'] = '{title_legend}, name, type;{source_legend}, foundation_archives, foundation_featured, categories_filter, categories_filter_type, sql_filter, foundation_order;{image_legend}, imgSize, imgSizeGallery;{filter_legend:hide}, categories;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['team_reader'] = '{title_legend}, name, type;{config_legend}, imgSize, imgSizeGallery;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['eval']['tl_class'] = 'w50 clr';
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['label'] = &$GLOBALS['TL_LANG']['tl_module']['jumpToOverride'];

$GLOBALS['TL_DCA']['tl_module']['config']['onload_callback'][] = array('tl_module_team', 'autoOverrideOptions');

/**
 * Class tl_module_team
 */
class tl_module_team extends Backend
{

    public function autoOverrideOptions($dc)
    {
        $objModuleItem = ModuleModel::findByPk($dc->id);

        if (!is_null($objModuleItem) && !is_null($objModuleItem->type) && stristr($objModuleItem->type, 'team')) {

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives']['options_callback'] = array('tl_module_team', 'getArchives');

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template']['options_callback'] = array('tl_module_team', 'getTemplates');

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection']['options_callback'] = array('tl_module_team', 'getSelectionOptions');
        }

    }

    public function getArchives(DataContainer $dc)
    {
        $arrArchives = array();
        $colArchives = TeamArchiveModel::findAll();

        while ($colArchives->next()) {
            $arrArchives[$colArchives->id] = $colArchives->title;
        }

        return $arrArchives;
    }

    public function getSelectionOptions(Contao\DataContainer $dc)
    {
        // If multiple, unserialize
        if (is_string($dc->activeRecord->foundation_archives)) {
            $arrArchives = unserialize($dc->activeRecord->foundation_archives);
        } else {
            $arrArchives = $dc->activeRecord->foundation_archives;
        }

        // Get all Items
        $arrItems = array();
        if (!empty($arrArchives)) {
            $colItems = TeamModel::findPublishedByPids($arrArchives);

            if (is_object($colItems)) {
                foreach ($colItems as $objItem) {
                    $arrItems[$objItem->id] = $objItem->title;
                }
            }
        }

        return $arrItems;
    }

    public function getTemplates(DataContainer $dc)
    {
        return $this->getTemplateGroup('team_');
    }
}
