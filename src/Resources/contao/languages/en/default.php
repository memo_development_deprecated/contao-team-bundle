<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */

$GLOBALS['TL_LANG']['MSC']['phone'] = 'Phone number';
$GLOBALS['TL_LANG']['MSC']['mobile'] = 'Mobile phone number';
$GLOBALS['TL_LANG']['MSC']['email'] = 'E-Mail';
