<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['memo_team'] = array('Personen', 'Teams / Personen verwalten');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['team_listing'] = array('Team Listenansicht');
$GLOBALS['TL_LANG']['FMD']['team_reader'] = array('Team Detailansicht');
$GLOBALS['TL_LANG']['FMD']['memo_team'] = array('Team / Personen');
