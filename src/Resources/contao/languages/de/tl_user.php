<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_user']['team_legend'] = 'Team-Rechte';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_user']['teams'] = array('Erlaubte Archive', 'Hier können Sie den Zugriff auf ein oder mehrere Team-Archive erlauben.');
$GLOBALS['TL_LANG']['tl_user']['teamp'] = array('Archivrechte', 'Hier können Sie die Archivrechte festlegen.');
