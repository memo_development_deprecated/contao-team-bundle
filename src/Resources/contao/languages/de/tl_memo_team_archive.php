<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_team_archive']['general_legend'] = 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_team_archive']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_memo_team_archive']['meta_legend'] = 'Metadaten';
$GLOBALS['TL_LANG']['tl_memo_team_archive']['publish_legend'] = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_team_archive']['title'] = array('Titel', 'Name des Teams');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['alias'] = array('Alias', 'Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird als URL Alias verwendet.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['jumpTo'] = array('Detailseite', 'Auf welcher Seite wurde das Reader-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['jumpToListing'] = array('Listenseiten', 'Auf welcher Seite wurde das Listen-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['category_groups'] = array('Kategorie Filter', 'Grenzen Sie die verfügbaren Kategorie-Gruppen für Kindelemente dieses Archives ein. Kind-Elemente können nur Kategorien von den hier definierten Gruppen erhalten. Leer = Alle Kategorien sind verfügbar.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['description'] = array('Beschreibung', 'Beschreibung des Eintrags. Wird auf der Detailseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['teaser'] = array('Teaser', 'Kurze Beschreibung des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['singleSRC'] = array('Bild', 'Bild des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['robots'] = array('Robotstag', 'Indexierungstag für Suchmaschinen');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['published'] = array('Veröffentlicht', 'Soll dieser Eintrag auf der Webseite veröffentlicht werden.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['start'] = array('Anzeigen ab', 'Wenn Sie das Team erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['stop'] = array('Anzeigen bis', 'Wenn Sie das Team nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_team_archive']['new'] = array('Neues Team', 'Neuen Team anlegen');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['show'] = array('Details', 'Infos zum Team mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['edit'] = array('Team bearbeiten ', 'Team bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['children'] = array('Personen bearbeiten ', 'Personen bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['cut'] = array('Team Verschieben', 'Team mit der ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['copy'] = array('Team Duplizieren ', 'Team mit der ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['toggle'] = array('Team Veröffentlichen ', 'Team mit der ID %s veröffentlichen');
$GLOBALS['TL_LANG']['tl_memo_team_archive']['delete'] = array('Team Löschen ', 'Team mit der ID %s löschen');
