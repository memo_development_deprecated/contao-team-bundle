<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_user_group']['team_legend'] = 'Team-Rechte';
