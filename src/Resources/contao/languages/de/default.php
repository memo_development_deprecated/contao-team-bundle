<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */

$GLOBALS['TL_LANG']['MSC']['phone'] = 'Telefonnummer';
$GLOBALS['TL_LANG']['MSC']['mobile'] = 'Mobiltelefonnummer';
$GLOBALS['TL_LANG']['MSC']['email'] = 'E-Mail';
$GLOBALS['TL_LANG']['MSC']['teamPicker'] = 'Team';

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['team'] = 'Team';
$GLOBALS['TL_LANG']['CTE']['team_listing_custom'] = array('Team Liste (Manuell)', 'Team Mitglieder können hier auch ausserhalb der Archive mit eigener Sortierung ausgegeben werden.');
$GLOBALS['TL_LANG']['CTE']['team_listing_archive'] = array('Team Liste (Filter)', 'Ausgabe der Team Mitglieder nach Archiv, Kategorie oder SQL Filter.');
