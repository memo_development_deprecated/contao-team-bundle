<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_team']['general_legend'] = 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_team']['contact_legend'] = 'Kontaktdaten';
$GLOBALS['TL_LANG']['tl_memo_team']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_memo_team']['meta_legend'] = 'Metadaten';
$GLOBALS['TL_LANG']['tl_memo_team']['publish_legend'] = 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_memo_team']['date_legend'] = 'Datum';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_team']['title'] = array('Vor- und Nachname', 'Vor- und Nachname der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['lastname'] = array('Nachname', 'Nachname der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['firstname'] = array('Vorname', 'Vorname der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['subtitle'] = array('Bezeichnung/Titel', 'Bezeichnung, Titel, Position der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['alias'] = array('Alias', 'Wie soll der Alias der Person lauten? Der Alias muss eindeutig sein und wird für die URL verwendet.');
$GLOBALS['TL_LANG']['tl_memo_team']['phone'] = array('Telefon', 'Direktwahl der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['mobile'] = array('Mobile', 'Handynummer der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['email'] = array('E-Mail', 'E-Mail Adresse der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['description'] = array('Beschreibung', 'Eine Beschreibung für eine eventuelle Detailseite');
$GLOBALS['TL_LANG']['tl_memo_team']['categories'] = array('Kategorie(n)', 'Hinterlegen Sie die entsprechende/n Kategorie/n für diese Person');
$GLOBALS['TL_LANG']['tl_memo_team']['category_id'] = array('Kategorie', 'Kategorie die zugewiesen wird.');
$GLOBALS['TL_LANG']['tl_memo_team']['category_fk_meta'] = array('Kategorie Zusatz', 'Falls Zusatzdaten pro Kategorie-Zuweisung benötigt werden, können diese hier hinterlegt werden.');
$GLOBALS['TL_LANG']['tl_memo_team']['singleSRC'] = array('Bild', 'Bild der Person');
$GLOBALS['TL_LANG']['tl_memo_team']['multiSRC'] = array('Galeriebilder', 'Bilder für eine Galerie / Slider etc.');
$GLOBALS['TL_LANG']['tl_memo_team']['seo_title'] = array('SEO Titel', 'Suchmaschinen Titel der Person - hiermit kann der Standard-Titel überschrieben werden. Optimaler Weise max. 60 Zeichen');
$GLOBALS['TL_LANG']['tl_memo_team']['seo_description'] = array('SEO Text', 'Suchmaschinen Text der Person - hiermit kann der Standard-Text überschrieben werden. Optimaler Weise max. 160 Zeichen');
$GLOBALS['TL_LANG']['tl_memo_team']['serpPreview'] = array('Google Suchergebnis-Vorschau', 'Hier können Sie sehen wie Google die Metadaten in den Suchergebnissen anzeigt. Andere Suchmaschinen zeigen gegebenenfalls längere Texte an oder beschneiden diese an einer anderen Position.');
$GLOBALS['TL_LANG']['tl_memo_team']['date'] = array('Eintrittsdatum', 'Eintrittsdatum der Person. Wird für die Sortierung verwendet bei Auswahl der entsprechenden Option.');
$GLOBALS['TL_LANG']['tl_memo_team']['published'] = array('Veröffentlicht', 'Soll dieser Person auf der Webseite veröffentlicht werden?');
$GLOBALS['TL_LANG']['tl_memo_team']['featured'] = array('Hervorgehoben', 'Soll diese Person auf der Webseite hervorgehoben werden?');
$GLOBALS['TL_LANG']['tl_memo_team']['start'] = array('Anzeigen ab', 'Wenn Sie das Teammitglied erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_team']['stop'] = array('Anzeigen bis', 'Wenn Sie das Teammitglied nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_team']['new'] = array('Neue Person', 'Neue Person anlegen');
$GLOBALS['TL_LANG']['tl_memo_team']['show'] = array('Details', 'Infos zur Person mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_team']['edit'] = array('Person bearbeiten ', 'Person bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_team']['cut'] = array('Person Verschieben', 'Person verschieben');
$GLOBALS['TL_LANG']['tl_memo_team']['copy'] = array('Person Duplizieren ', 'Person duplizieren');
$GLOBALS['TL_LANG']['tl_memo_team']['toggle'] = array('Person Veröffentlichen ', 'Person veröffentlichen');
$GLOBALS['TL_LANG']['tl_memo_team']['delete'] = array('Person Löschen ', 'Person löschen');
$GLOBALS['TL_LANG']['tl_memo_team']['feature'] = array('Person Hervorheben ', 'Person hervorheben');
