<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\Model;

use Memo\FoundationBundle\Model\FoundationModel;

/**
 * Class TeamModel
 * @package Memo\TeamBundle\Model
 */
class TeamModel extends FoundationModel
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_memo_team';
}
