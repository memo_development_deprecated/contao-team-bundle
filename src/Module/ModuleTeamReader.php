<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\Module;

use Contao\Input;
use Memo\FoundationBundle\Module\FoundationModule;
use Memo\TeamBundle\Model\TeamModel;

class ModuleTeamReader extends FoundationModule
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_team_reader';

    /**
     * Generate module
     */
    protected function compile()
    {

        // Get auto_item (GET Parameter)
        $strAlias = Input::get('auto_item');

        // Check if there is an Alias in the URL
        if ($strAlias !== '') {

            // Get the item by alias (this also filters by start/stop, looks for alias_en etc.)
            $colItem = TeamModel::getItemByTranslatedAlias($strAlias);

            // Set the MetaData for the detailpage
            if ($colItem) {
                $this->setMetaData($colItem);

                // Parse the item into an array (this also generates images, html-template etc.)
                $arrItems = $this->parseItems($colItem, null, false);

                // Define the data "raw" for custom template use
                $this->Template->setData($colItem->row());

                // Set the custom template, if defined
                if ($this->customTpl) {
                    $this->Template->strTemplate = $this->customTpl;
                }

                // Pass the items to the template
                $this->Template->items = $arrItems;

                // Template CSS ID
                $this->Template->class = trim('mod_' . $this->type . ' ' . ($this->cssID[1] ?? ''));
                $this->Template->cssID = !empty($this->cssID[0]) ? ' id="' . $this->cssID[0] . '"' : '';

                // Parse the Template
                $this->Template->parse();

            }
        }
    }
}
