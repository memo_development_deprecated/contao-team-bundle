<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\Module;

use Memo\FoundationBundle\Module\FoundationModule;
class ModuleTeamListing extends FoundationModule
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_team_listing';
}
