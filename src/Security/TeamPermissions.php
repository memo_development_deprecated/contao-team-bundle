<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\Security;

final class TeamPermissions
{
    public const USER_CAN_EDIT_ARCHIVE = 'contao_user.teams';
    public const USER_CAN_CREATE_ARCHIVES = 'contao_user.teamp.create';
    public const USER_CAN_DELETE_ARCHIVES = 'contao_user.teamp.delete';
}
