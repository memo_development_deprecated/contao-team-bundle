<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\Picker;

use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\CoreBundle\Picker\AbstractInsertTagPickerProvider;
use Contao\CoreBundle\Picker\DcaPickerProviderInterface;
use Contao\CoreBundle\Picker\PickerConfig;
use Knp\Menu\FactoryInterface;
use Memo\TeamBundle\Model\TeamArchiveModel;
use Memo\TeamBundle\Model\TeamModel;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class TeamPickerProvider extends AbstractInsertTagPickerProvider implements DcaPickerProviderInterface, FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    public function __construct(
        FactoryInterface          $menuFactory,
        RouterInterface           $router,
        TranslatorInterface|null  $translator,
        private readonly Security $security,
    )
    {
        parent::__construct($menuFactory, $router, $translator);
    }

    public function getName(): string
    {
        return 'teamPicker';
    }

    public function supportsContext($context): bool
    {
        return 'link' === $context;
    }

    public function supportsValue(PickerConfig $config): bool
    {

        return $this->isMatchingInsertTag($config);
    }

    public function getDcaTable(PickerConfig|null $config = null): string
    {
        return 'tl_memo_team';
    }

    public function getDcaAttributes(PickerConfig $config): array
    {

        $attributes = ['fieldType' => 'radio'];

        if ($this->supportsValue($config)) {
            $attributes['value'] = $this->getInsertTagValue($config);

            if ($flags = $this->getInsertTagFlags($config)) {
                $attributes['flags'] = $flags;
            }
        }

        return $attributes;
    }


    public function convertDcaValue(PickerConfig $config, $value): string
    {
        return sprintf($this->getInsertTag($config), $value);
    }


    protected function getRouteParameters(PickerConfig $config = null): array
    {
        $params = ['do' => 'memo_team'];

        if (null === $config || !$config->getValue() || !$this->supportsValue($config)) {
            return $params;
        }

        if (null !== ($intTeamId = $this->getTeamId($this->getInsertTagValue($config)))) {
            $params['table'] = 'tl_memo_team';
            $params['id'] = $intTeamId;
        }

        return $params;
    }

    protected function getDefaultInsertTag(): string
    {
        return '{{team_url::%s}}';
    }

    private function getTeamId(int|string $id): int|null
    {
        $eventAdapter = $this->framework->getAdapter(TeamModel::class);

        if (!($TeamModel = $eventAdapter->findByPk($id)) instanceof TeamModel) {
            return null;
        }

        if (!($teamArchive = $TeamModel->getRelated('pid')) instanceof TeamArchiveModel) {
            return null;
        }

        return (int)$teamArchive->id;
    }
}
