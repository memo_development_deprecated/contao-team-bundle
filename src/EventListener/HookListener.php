<?php declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\EventListener;

use Terminal42\ChangeLanguage\Event\ChangelanguageNavigationEvent;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Memo\TeamBundle\Model\TeamModel;
use Memo\TeamBundle\Model\TeamArchiveModel;
use Memo\FoundationBundle\EventListener\FoundationHookListener;
use Contao\Input;

class HookListener extends FoundationHookListener
{
    /**
     * @Hook("changelanguageNavigation")
     */
    public function onChangelanguageNavigation(ChangelanguageNavigationEvent $objEvent)
    {
        // Gather some information
        $objTargetRoot = $objEvent->getNavigationItem()->getRootPage();
        $objTarget = $objEvent->getNavigationItem()->getTargetPage();
        $strTargetLanguage = $objTargetRoot->rootLanguage;
        $strAlias = Input::get('auto_item');
        $arrDetailPageIds = TeamArchiveModel::getDetailPages(true, true);
        $objNavigationItem = $objEvent->getNavigationItem();

        // Detect item for the current page, e.g. to do nothing
        if ($objNavigationItem->isCurrentPage()) {
            return;
        }

        // Only act on auto_item and on jumpToPages
        if ($strAlias != '' && in_array($objTarget->id, $arrDetailPageIds)) {
            // Get the Item
            if ($colItem = TeamModel::getItemByTranslatedAlias($strAlias)) {
                $colItem[0]->getTranslatedModel($strTargetLanguage);

                if ($colItem[0]->alias != '') {
                    if ($objEvent->getUrlParameterBag()->hasUrlAttribute('items')) {
                        $objEvent->getUrlParameterBag()->setUrlAttribute('items', $colItem[0]->alias);
                    } else {
                        $objEvent->getUrlParameterBag()->setUrlAttribute('alias', $colItem[0]->alias);
                    }
                }
            }

        }
    }
}
