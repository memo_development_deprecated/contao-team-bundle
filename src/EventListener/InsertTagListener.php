<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoTeamBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\TeamBundle\EventListener;

use Contao\Controller;
use Memo\TeamBundle\Model\TeamArchiveModel;
use Memo\TeamBundle\Model\TeamModel;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\System;
use Contao\PageModel;

class InsertTagListener
{
    /**
     * @Hook("replaceInsertTags")
     */
    public function __invoke(
        string $insertTag,
        bool   $useCache,
        string $cachedValue,
        array  $flags,
        array  $tags,
        array  $cache,
        int    $_rit,
        int    $_cnt
    )
    {
        if (stristr($insertTag, 'team_url')) {

            // Get ID from Inserttag
            $arrElements = explode('::', $insertTag);
            $intID = $arrElements[1];

            // ID defined
            if ($intID) {

                // Try and get Item
                if ($objTeamMember = TeamModel::findById($intID)) {

                    return $objTeamMember->getURL();
                }
            }
        }

        return false;
    }
}
